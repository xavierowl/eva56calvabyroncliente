package ec.edu.ups.vista;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import ec.edu.ups.modelos.Cliente;
import ec.edu.ups.modelos.Cuenta;
import ec.edu.ups.on.GestionBancoRemote;

import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.awt.event.ActionEvent;
import javax.swing.border.LineBorder;
import java.awt.Color;

public class AgregarCliente extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3995866284292340193L;
	
	private JPanel contentPane;
	private JTextField txtCedula;
	private JTextField txtTipoCuenta;
	private JTextField txtSaldoCuenta;
	private GestionBancoRemote remota;
	private JTextField txtNombres;
	private Cliente cliente;
	private Cuenta cuenta;
	private JTextField txtApellidos;
	private JTextField txtFecNac;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AgregarCliente frame = new AgregarCliente();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	
	public void setEJB(GestionBancoRemote remota) {
		this.remota = remota;
	}
	
	public AgregarCliente() {
		setTitle("Agregar Contacto");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 353, 386);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		setLocationRelativeTo(null);
		
		JPanel panel = new JPanel();
		panel.setBorder(new LineBorder(new Color(0, 0, 0)));
		panel.setBounds(186, 13, 136, 149);
		contentPane.add(panel);
		panel.setLayout(null);
		
		txtTipoCuenta = new JTextField();
		txtTipoCuenta.setBounds(12, 60, 116, 22);
		panel.add(txtTipoCuenta);
		txtTipoCuenta.setColumns(10);
		
		JLabel lblNewLabel_1 = new JLabel("Tipo:");
		lblNewLabel_1.setBounds(12, 44, 56, 16);
		panel.add(lblNewLabel_1);
		
		JLabel lblNewLabel_2 = new JLabel("Saldo:");
		lblNewLabel_2.setBounds(12, 85, 56, 16);
		panel.add(lblNewLabel_2);
		
		txtSaldoCuenta = new JTextField();
		txtSaldoCuenta.setBounds(12, 103, 116, 22);
		panel.add(txtSaldoCuenta);
		txtSaldoCuenta.setColumns(10);
		
		JLabel lblNewLabel_7 = new JLabel("Cuenta");
		lblNewLabel_7.setBounds(42, 13, 47, 16);
		panel.add(lblNewLabel_7);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBorder(new LineBorder(new Color(0, 0, 0)));
		panel_1.setBounds(12, 13, 147, 261);
		contentPane.add(panel_1);
		panel_1.setLayout(null);
		
		JLabel lblNewLabel_3 = new JLabel("Cliente");
		lblNewLabel_3.setBounds(50, 13, 56, 16);
		panel_1.add(lblNewLabel_3);
		
		txtCedula = new JTextField();
		txtCedula.setBounds(12, 59, 123, 22);
		panel_1.add(txtCedula);
		txtCedula.setColumns(10);
		
		JLabel lblNewLabel = new JLabel("C\u00E9dula:");
		lblNewLabel.setBounds(12, 42, 56, 16);
		panel_1.add(lblNewLabel);
		
		JLabel lblNewLabel_4 = new JLabel("Nombres:");
		lblNewLabel_4.setBounds(12, 83, 56, 16);
		panel_1.add(lblNewLabel_4);
		
		txtNombres = new JTextField();
		txtNombres.setBounds(12, 106, 123, 22);
		panel_1.add(txtNombres);
		txtNombres.setColumns(10);
		
		JLabel lblNewLabel_5 = new JLabel("Apellidos");
		lblNewLabel_5.setBounds(12, 141, 56, 16);
		panel_1.add(lblNewLabel_5);
		
		txtApellidos = new JTextField();
		txtApellidos.setBounds(12, 159, 123, 22);
		panel_1.add(txtApellidos);
		txtApellidos.setColumns(10);
		
		JLabel lblNewLabel_6 = new JLabel("Fecha de nacimiento:");
		lblNewLabel_6.setBounds(12, 190, 123, 16);
		panel_1.add(lblNewLabel_6);
		
		txtFecNac = new JTextField();
		txtFecNac.setBounds(12, 219, 116, 22);
		panel_1.add(txtFecNac);
		txtFecNac.setColumns(10);
		
		JButton btnNewButton = new JButton("Agregar");
		btnNewButton.setBounds(122, 287, 97, 25);
		contentPane.add(btnNewButton);
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					if(!txtCedula.getText().equals("") && !txtTipoCuenta.getText().equals("") && !txtSaldoCuenta.getText().equals("")
							&& !txtNombres.getText().equals("") && !txtApellidos.getText().equals("")
							&& !txtFecNac.getText().equals("")) {
						cuenta = new Cuenta();
						List<Cuenta> cuentas = new ArrayList<Cuenta>();
						cliente = new Cliente();
						cliente.setCli_nombres(txtNombres.getText());
						cliente.setCli_apellidos(txtApellidos.getText());
						cliente.setCli_cedula(txtCedula.getText());
						cliente.setCli_fec_nac(txtFecNac.getText());
						cuenta.setCue_tipo(txtTipoCuenta.getText());
						cuenta.setCue_saldo(Double.valueOf(txtSaldoCuenta.getText()));
						cuentas.add(cuenta);
						cliente.setCli_cuentas(cuentas);
						remota.crearCliente(cliente);
					}
					else {
						JOptionPane.showMessageDialog(null, "Uno o m�s campos se encuentran vac�os.");
					}
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
	}
}
