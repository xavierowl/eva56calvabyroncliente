package ec.edu.ups.vista;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import ec.edu.ups.modelos.Cliente;
import ec.edu.ups.modelos.Cuenta;
import ec.edu.ups.on.GestionBancoRemote;

import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.awt.event.ActionEvent;
import javax.swing.border.LineBorder;
import java.awt.Color;

public class AgregarCuenta extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3995866284292340193L;
	
	private JPanel contentPane;
	private JTextField txtTipoCuenta;
	private JTextField txtSaldoCuenta;
	private GestionBancoRemote remota;
	private Cliente cliente;
	private Cuenta cuenta;
	private JTextField txtCedula;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AgregarCuenta frame = new AgregarCuenta();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	
	public void setEJB(GestionBancoRemote remota) {
		this.remota = remota;
	}
	
	public AgregarCuenta() {
		setTitle("Agregar Contacto");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 325, 337);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		setLocationRelativeTo(null);
		
		JPanel panel = new JPanel();
		panel.setBorder(new LineBorder(new Color(0, 0, 0)));
		panel.setBounds(79, 13, 136, 195);
		contentPane.add(panel);
		panel.setLayout(null);
		
		txtTipoCuenta = new JTextField();
		txtTipoCuenta.setBounds(12, 117, 116, 22);
		panel.add(txtTipoCuenta);
		txtTipoCuenta.setColumns(10);
		
		JLabel lblNewLabel_1 = new JLabel("Tipo:");
		lblNewLabel_1.setBounds(12, 101, 56, 16);
		panel.add(lblNewLabel_1);
		
		JLabel lblNewLabel_2 = new JLabel("Saldo:");
		lblNewLabel_2.setBounds(12, 142, 56, 16);
		panel.add(lblNewLabel_2);
		
		txtSaldoCuenta = new JTextField();
		txtSaldoCuenta.setBounds(12, 160, 116, 22);
		panel.add(txtSaldoCuenta);
		txtSaldoCuenta.setColumns(10);
		
		JLabel lblNewLabel = new JLabel("Cuenta");
		lblNewLabel.setBounds(45, 15, 40, 16);
		panel.add(lblNewLabel);
		
		JLabel lblNewLabel_3 = new JLabel("C\u00E9dula:");
		lblNewLabel_3.setBounds(12, 44, 56, 16);
		panel.add(lblNewLabel_3);
		
		txtCedula = new JTextField();
		txtCedula.setBounds(12, 66, 116, 22);
		panel.add(txtCedula);
		txtCedula.setColumns(10);
		
		JButton btnNewButton = new JButton("Agregar");
		btnNewButton.setBounds(105, 221, 97, 25);
		contentPane.add(btnNewButton);
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					if(!txtCedula.getText().equals("") && !txtTipoCuenta.getText().equals("") && !txtSaldoCuenta.getText().equals("")) {
						cuenta = new Cuenta();
						List<Cuenta> cuentas = new ArrayList<Cuenta>();
						cliente = new Cliente();
						cliente.setCli_id(remota.buscarCliente(txtCedula.getText()).getCli_id());
						cuenta.setCue_tipo(txtTipoCuenta.getText());
						cuenta.setCue_saldo(Double.valueOf(txtSaldoCuenta.getText()));
						cuenta.setCue_cli(cliente);
						cuentas.add(cuenta);
						cliente.setCli_cuentas(cuentas);
						remota.crearCuenta(cuenta);
						JOptionPane.showMessageDialog(null, "Cuenta registrada");
					}
					else {
						JOptionPane.showMessageDialog(null, "Uno o m�s campos se encuentran vac�os.");
					}
				} catch (Exception e1) {
					JOptionPane.showMessageDialog(null, e1);
				}
			}
		});
	}
}
