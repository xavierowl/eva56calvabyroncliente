package ec.edu.ups.vista;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.transaction.Transactional.TxType;

import ec.edu.ups.modelos.Cliente;
import ec.edu.ups.modelos.Cuenta;
import ec.edu.ups.on.GestionBancoRemote;

import javax.swing.JTextPane;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTextField;

public class Main extends JFrame {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1381298481257985469L;
	public static GestionBancoRemote remota;
	public GestionBancoRemote remotaInstancia;

	public void instanciarEJB() throws Exception {
		try {  
            final Hashtable<String, Comparable> jndiProperties =  
                    new Hashtable<String, Comparable>();  
            jndiProperties.put(Context.INITIAL_CONTEXT_FACTORY,  
                    "org.wildfly.naming.client.WildFlyInitialContextFactory");  
            jndiProperties.put("jboss.naming.client.ejb.context", true);  
              
            jndiProperties.put(Context.PROVIDER_URL, "http-remoting://localhost:8080");  
            jndiProperties.put(Context.SECURITY_PRINCIPAL, "ejbApp");  
            jndiProperties.put(Context.SECURITY_CREDENTIALS, "ejbApp"); 
              
            final Context context = new InitialContext(jndiProperties);
              
            final String lookupName = "ejb:/Eva56CalvaByronServidor/GestionBanco!ec.edu.ups.on.GestionBancoRemote";
              
            this.remotaInstancia = (GestionBancoRemote) context.lookup(lookupName);  
              
        } catch (Exception ex) {
        	System.out.println("Se ah producido un error!!!!!!!:");
            ex.printStackTrace();
            throw ex;
        }  
	}

	private JPanel contentPane;
	private JTextField txtCedula;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		Main main = new Main();
		try {
			main.instanciarEJB();
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "Se ah producido un error al instanciar el bean empresarial: "+e.getMessage());
			}
		remota = main.remotaInstancia;
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Main frame = new Main();
					frame.setLocationRelativeTo(null);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 * @throws Exception 
	 */
	public Main() {
		setTitle("Gesti\u00F3n Bancaria");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 415, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		final JTextPane txtClientes = new JTextPane();
		txtClientes.setBounds(12, 43, 203, 197);
		contentPane.add(txtClientes);
		
		JLabel lblNewLabel = new JLabel("Clientes");
		lblNewLabel.setBounds(83, 14, 56, 16);
		contentPane.add(lblNewLabel);
		
		JButton btnAgregarCliente = new JButton("Nuevo Cliente");
		btnAgregarCliente.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				AgregarCliente ac = new AgregarCliente();
				ac.setEJB(remota);
				ac.setVisible(true);
			}
		});
		btnAgregarCliente.setBounds(244, 43, 129, 25);
		contentPane.add(btnAgregarCliente);
		
		JButton btnAgregarCuenta = new JButton("Nueva Cuenta");
		btnAgregarCuenta.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				AgregarCuenta acue = new AgregarCuenta();
				acue.setEJB(remota);
				acue.setVisible(true);
			}
		});
		btnAgregarCuenta.setBounds(244, 81, 129, 25);
		contentPane.add(btnAgregarCuenta);
		
		txtClientes.setText("Cliente		Cuentas\n");
		
		JButton btnBuscarCliente = new JButton("Buscar Cliente");
		btnBuscarCliente.setBounds(244, 196, 129, 25);
		contentPane.add(btnBuscarCliente);
		
		txtCedula = new JTextField();
		txtCedula.setBounds(244, 161, 129, 22);
		contentPane.add(txtCedula);
		txtCedula.setColumns(10);
		
		JLabel lblNewLabel_1 = new JLabel("C\u00E9dula:");
		lblNewLabel_1.setBounds(283, 136, 56, 16);
		contentPane.add(lblNewLabel_1);
		
		btnBuscarCliente.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(!txtCedula.getText().isEmpty()) {
					Cliente cliente = new Cliente();
					try {
						cliente = remota.buscarCliente(txtCedula.getText());
						txtClientes.setText("Cliente		Cuentas\n"+cliente.getCli_nombres()+"\n");
						txtClientes.setText(txtClientes.getText()+cliente.getCli_apellidos()+"\n");
						txtClientes.setText(txtClientes.getText()+cliente.getCli_cedula()+"\n");
						txtClientes.setText(txtClientes.getText()+cliente.getCli_fec_nac()+"\n");
						for (Cuenta cuenta : cliente.getCli_cuentas()) {
							txtClientes.setText("	"+txtClientes.getText()+"\n"+cuenta.getCue_id()+"	"+cuenta.getCue_tipo());
						}
					} catch (Exception e1) {
						JOptionPane.showMessageDialog(null, e1.getMessage());
					}
				}
				else {
					JOptionPane.showMessageDialog(null, "El campo de la c�dula est� vac�o.");
				}
			}
		});
		
		try {
			for (Cliente cliente : remota.listarClientes()) {
				txtClientes.setText(txtClientes.getText()+"\n"+cliente.getCli_nombres());
				for (Cuenta cuenta : cliente.getCli_cuentas()) {
					txtClientes.setText("	"+txtClientes.getText()+"\n"+cuenta.getCue_id()+"	"+cuenta.getCue_tipo());
				}
			}
		} catch (Exception e) {
			
			}
	}
}
