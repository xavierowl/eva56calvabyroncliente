package ec.edu.ups.modelos;

import java.io.Serializable;

public class Cuenta implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -6312526445010477528L;

	private int cue_id;
	
	private double cue_saldo;
	
	private String cue_tipo;
	
	private Cliente cue_cliente;
	
	public int getCue_id() {
		return cue_id;
	}
	public void setCue_id(int cue_id) {
		this.cue_id = cue_id;
	}
	public double getCue_saldo() {
		return cue_saldo;
	}
	public void setCue_saldo(double cue_saldo) {
		this.cue_saldo = cue_saldo;
	}
	public String getCue_tipo() {
		return cue_tipo;
	}
	public void setCue_tipo(String cue_tipo) {
		this.cue_tipo = cue_tipo;
	}
	public Cliente getCue_cli() {
		return cue_cliente;
	}
	public void setCue_cli(Cliente cue_cli) {
		this.cue_cliente = cue_cli;
	}
}
