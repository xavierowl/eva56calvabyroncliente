package ec.edu.ups.modelos;

import java.io.Serializable;
import java.util.List;

public class Cliente implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 5489502601528169368L;
	private int cli_id;
	private String cli_cedula;
	private String cli_nombres;
	private String cli_apellidos;
	private String cli_fec_nac;
	
	private List<Cuenta> cli_cuentas;
	public int getCli_id() {
		return cli_id;
	}
	public void setCli_id(int cli_id) {
		this.cli_id = cli_id;
	}
	public String getCli_cedula() {
		return cli_cedula;
	}
	public void setCli_cedula(String cli_cedula) {
		this.cli_cedula = cli_cedula;
	}
	public String getCli_nombres() {
		return cli_nombres;
	}
	public void setCli_nombres(String cli_nombres) {
		this.cli_nombres = cli_nombres;
	}
	public String getCli_apellidos() {
		return cli_apellidos;
	}
	public void setCli_apellidos(String cli_apellidos) {
		this.cli_apellidos = cli_apellidos;
	}
	public String getCli_fec_nac() {
		return cli_fec_nac;
	}
	public void setCli_fec_nac(String cli_fec_nac) {
		this.cli_fec_nac = cli_fec_nac;
	}
	public List<Cuenta> getCli_cuentas() {
		return cli_cuentas;
	}
	public void setCli_cuentas(List<Cuenta> cli_cuentas) {
		this.cli_cuentas = cli_cuentas;
	}
	@Override
	public String toString() {
		return "Cliente [cli_id=" + cli_id + ", cli_cedula=" + cli_cedula + ", cli_nombres=" + cli_nombres
				+ ", cli_apellidos=" + cli_apellidos + ", cli_fec_nac=" + cli_fec_nac + ", cli_cuentas=" + cli_cuentas
				+ "]";
	}
}
